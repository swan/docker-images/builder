FROM openjdk:8-jre-alpine
MAINTAINER Diogo Castro <diogo.castro@cern.ch>

ARG SBT_VERSION=1.2.8

RUN apk add --update --no-cache \
                    curl \
                    bash \
                    git \
                    make \
                    nodejs \
                    nodejs-npm \
                    yarn \
    && \
    npm install -g \
                    less \
                    less-plugin-clean-css \
    && \
    curl -fsL https://github.com/sbt/sbt/releases/download/v$SBT_VERSION/sbt-$SBT_VERSION.tgz | tar xfz - -C /usr/local && \
    ln -s /usr/local/sbt/bin/* /usr/local/bin/ && \
    sbt sbtVersion \
    && \
    apk del curl

WORKDIR /root
CMD ["/bin/sh"]
